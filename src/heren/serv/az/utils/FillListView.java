/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.utils;

import heren.serv.az.order.item.detail.ItemModal;
import heren.serv.az.order.listItem.ListItemController;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 *
 * @author rasha_000
 */
public class FillListView {

    public static void fillList(int from, int to, VBox vboxlist) {
        vboxlist.getChildren().clear();
        for (int i = from; i < to; i++) {
            final ListItemController lic = new ListItemController();
            lic.setOnMouseEntered(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    lic.setStyle("-fx-border-color:blue;");
                }
            });

            lic.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    lic.setStyle("-fx-border-color:white;");
                }
            });
            lic.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    lic.setStyle("-fx-border-color:red;");
                    ItemModal modal = new ItemModal(lic);

                }
            });
            lic.setPrefWidth(vboxlist.getPrefWidth());
            vboxlist.getChildren().add(lic);
        }
    }
}
