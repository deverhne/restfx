/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.utils;

/**
 *
 * @author Rashad Javadov
 */
public class FXMLPATH {

    public static String loginFXML = "/heren/serv/az/login/login.fxml";
    public static String orderFXML = "/heren/serv/az/order/OrderPane.fxml";
    public static String departmentFXML = "/heren/serv/az/dep/DepartmentPane.fxml";
    public static String setAdditional = "/heren/serv/az/dep/setAdditional.fxml";
}
