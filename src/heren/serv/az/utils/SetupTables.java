/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.utils;

import heren.rest.common.client.CMyTable;
import heren.serv.az.dep.AdditionalPane;
import heren.serv.az.dep.DepartmentsController;
import heren.serv.az.dep.detail.TDetailButton;
import static heren.serv.az.fx.MainStage.mainStage;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author rasha_000
 */
public class SetupTables {

    public static void createTables(List<CMyTable> tables, FlowPane tDetails) {
        tDetails.getChildren().clear();
        for (CMyTable table : tables) {
            TDetailButton detailPane = new TDetailButton();
            detailPane.getBottomLabel().setStyle("-fx-background-color: green;");
            detailPane.getBottomLabel().setText(table.getTableNumber());
            detailPane.getCenterlabel().setText("name ");
            detailPane.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    AdditionalPane pane = new AdditionalPane();
                }
            });
            tDetails.getChildren().add(detailPane);
        }
    }
}
