/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.utils;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Mobby
 */
public class LabelSettigs {

    public static void setPassword(final Label btn, final PasswordField passw) {
        btn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                passw.setText(passw.getText() + btn.getText());
            }
        });
    }

    public static void resetPassword(final Label btn, final PasswordField passw) {
        btn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                passw.setText("");
            }
        });
    }
}
