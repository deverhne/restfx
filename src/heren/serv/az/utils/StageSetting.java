/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.utils;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Rashad Javadov
 */
public class StageSetting {

    public   void configureStage(Stage stage, String path) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();

        try {
            FXMLLoader fxml = new FXMLLoader(getClass().getResource(path));
            Parent root = (Parent) fxml.load();
            stage.setScene(new Scene(root, width, height));

        } catch (IOException ex) {
            Logger.getLogger(StageSetting.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
