/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.main;

import heren.serv.az.login.LoginController;
import heren.serv.az.utils.FXMLPATH;
import java.awt.Dimension;
import java.awt.Toolkit;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Mobby
 */
public class FXdesktopMain extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        
        FXMLLoader fxml = new FXMLLoader(getClass().getResource(FXMLPATH.loginFXML));
        Parent root = (Parent) fxml.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setFullScreen(true);

        LoginController loginController = (LoginController) fxml.getController();
        loginController.setMainStage(stage);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();
        
        stage.setWidth(width);
        stage.setHeight(height);

        stage.show();       
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
