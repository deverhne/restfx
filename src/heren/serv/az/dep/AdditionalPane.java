/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.dep;

import heren.serv.az.fx.MainStage;
import static heren.serv.az.fx.MainStage.mainStage;
import heren.serv.az.utils.FXMLPATH;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Rashad JavaDOC
 */
public class AdditionalPane extends MainStage implements Initializable {

    private Stage modal_dialog;
    @FXML
    TextField passw;

    public AdditionalPane() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/heren/serv/az/dep/setAdditional.fxml"));
            fxmlLoader.setController(this);
            Parent root = (Parent) fxmlLoader.load();
            Scene scene = new Scene(root);
            modal_dialog = new Stage(StageStyle.DECORATED);
            modal_dialog.initModality(Modality.WINDOW_MODAL);
            modal_dialog.initOwner(mainStage);
            modal_dialog.setScene(scene);
            modal_dialog.show();
        } catch (IOException ex) {
            Logger.getLogger(DepartmentsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void actionClick(MouseEvent event) {
        System.out.println(((Button) event.getSource()).getText());
        String lab = ((Button) event.getSource()).getText();
        System.out.println(lab);
        if (!lab.equalsIgnoreCase("")) {
            if (lab.equals("OK")) {
                modal_dialog.hide();
                settingStage.configureStage(mainStage, FXMLPATH.orderFXML);
            } else {
                passw.setText(passw.getText() + lab);
            }
        } else {
            passw.setText("");
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}
