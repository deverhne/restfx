/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.dep.detail;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author rasha_000
 */
public class DepButton extends AnchorPane {

    @FXML
    Label centerlabel,bottomLbl;

    public DepButton() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("depButton.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public Label getCenterlabel() {
        return centerlabel;
    }

    public void setCenterlabel(Label centerlabel) {
        this.centerlabel = centerlabel;
    }

    public Label getBottomLbl() {
        return bottomLbl;
    }

    public void setBottomLbl(Label bottomLbl) {
        this.bottomLbl = bottomLbl;
    }
    
}
