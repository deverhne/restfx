/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.dep.detail;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author rasha_000
 */
public class TDetailButton extends AnchorPane {

    @FXML
    Label centerlabel;
    @FXML
    Label bottomLabel;
    @FXML
    Label lastServ;
    @FXML
    Label pcount;

    public TDetailButton() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Tdetail.fxml"));
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

 
    public Label getCenterlabel() {
        return centerlabel;
    }

    public void setCenterlabel(Label centerlabel) {
        this.centerlabel = centerlabel;
    }

    public Label getBottomLabel() {
        return bottomLabel;
    }

    public void setBottomLabel(Label bottomLabel) {
        this.bottomLabel = bottomLabel;
    }

    public Label getLastServ() {
        return lastServ;
    }

    public void setLastServ(Label lastServ) {
        this.lastServ = lastServ;
    }

    public Label getPcount() {
        return pcount;
    }

    public void setPcount(Label pcount) {
        this.pcount = pcount;
    }

}
