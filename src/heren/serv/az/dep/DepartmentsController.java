/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.dep;

import heren.rest.common.client.CDepartment;
import heren.rest.common.client.CMyTable;
import heren.serv.az.dep.detail.DepButton;
import heren.serv.az.fx.ControlledScreen;
import heren.serv.az.fx.MainStage; 
import heren.serv.az.fx.ScreensController;
import heren.serv.az.utils.FXMLPATH;
import heren.serv.az.utils.SetupTables;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;

/**
 * FXML Controller class
 *
 * @author Rashad Javadov
 */
public class DepartmentsController extends MainStage implements Initializable, ControlledScreen {

    ScreensController myController;
    public List<CMyTable> tables;
    public List<CDepartment> deps;
    @FXML
    FlowPane tDetails;
    @FXML
    FlowPane depPanel;

 

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        BackButton backBtn = new BackButton();
        backBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                settingStage.configureStage(mainStage, FXMLPATH.loginFXML);
            }
        });
        depPanel.getChildren().add(backBtn);

        System.out.println(" deps   " + rs.getDepartments("3").size());
        deps = rs.getDepartments("3");

        for (final CDepartment dep : deps) {
            DepButton depButton = new DepButton();
//            depButton.getCenterlabel().setText("" + dep.getName());
            depButton.getBottomLbl().setText("Free : " + dep.getTablecount());
            depButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    tables = rs.getMyTablesInDepartment(dep.getId() + "");
                    SetupTables.createTables(tables, tDetails);
                }
            });
            depPanel.getChildren().add(depButton);
        }
        //on open set default first dep tables
        tables = rs.getMyTablesInDepartment(deps.get(0).getId() + "");
        SetupTables.createTables(tables, tDetails);

    }

    @Override
    public void setScreenParent(ScreensController screenParent) {
        myController = screenParent;
    }

}
