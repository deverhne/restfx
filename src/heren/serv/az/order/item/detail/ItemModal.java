/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.order.item.detail;

import heren.serv.az.dep.DepartmentsController;
import heren.serv.az.fx.MainStage;
import static heren.serv.az.fx.MainStage.mainStage;
import heren.serv.az.order.OrderController;
import heren.serv.az.order.listItem.ListItemController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Mobby
 */
public class ItemModal extends MainStage implements Initializable {

    @FXML
    Label minusLbl, plusLbl, numberLbl, deleteLbl, backLbl, okLbl;
    ListItemController licenc;
    Stage modal_dialog;

    public ItemModal(ListItemController lic) {
        try {
            licenc = lic;
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/heren/serv/az/order/item/detail/itemModal.fxml"));
            fxmlLoader.setController(this);
            Parent root = (Parent) fxmlLoader.load();
            Scene scene = new Scene(root);
            modal_dialog = new Stage(StageStyle.TRANSPARENT);
            modal_dialog.initModality(Modality.WINDOW_MODAL);
            modal_dialog.initOwner(mainStage);
            modal_dialog.setScene(scene);
            modal_dialog.show();

        } catch (IOException ex) {
            Logger.getLogger(DepartmentsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void deleteClick(MouseEvent event) {
        System.out.println("delete " + licenc.getFirstLab().getText());
        OrderController.vboxlist.getChildren().remove(licenc);
    }

    @FXML
    public void addClick(MouseEvent event) {
        System.out.println("addClick " + licenc.getFirstLab().getText());
        if (numberLbl.getText().equals("0.5")) {
            numberLbl.setText("1");
        } else {
            int count = Integer.parseInt(numberLbl.getText()) + 1;
            numberLbl.setText("" + count);
        }
//        OrderController.vboxlist.getChildren().l(licenc);
    }

    @FXML
    public void minusClick(MouseEvent event) {
        System.out.println("minusClick " + licenc.getFirstLab().getText());
//        int count = Integer.parseInt(numberLbl.getText());
        if (numberLbl.getText().equals("0.5")||numberLbl.getText().equals("1")) {
            numberLbl.setText("0.5");
        } else {
            int mc = Integer.parseInt(numberLbl.getText()) - 1;
            numberLbl.setText(mc + "");
        }
    }

    @FXML
    public void closeModal(MouseEvent event) {
        System.out.println("closeModal ");
        modal_dialog.hide();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        plusLbl.setOnMouseClicked(new EventHandler<MouseEvent>() {
//            @Override
//            public void handle(MouseEvent event) {
//                System.out.println("hereeeeeeeeeeeeeeeeeee" + numberLbl.getText());
//                int num = Integer.valueOf(numberLbl.getText()) + 1;
//                numberLbl.setText("" + num);
//            }
//        });
    }
}
