package heren.serv.az.order;

import heren.rest.common.client.CScreenMenu;
import heren.rest.common.client.CScreenMenuCategory;
import heren.serv.az.category.CategoryButton;
import heren.serv.az.category.SubCategoryButton;
import heren.serv.az.fx.ControlledScreen;
import heren.serv.az.fx.MainStage;
import static heren.serv.az.fx.MainStage.mainStage;
import static heren.serv.az.fx.MainStage.settingStage;
import heren.serv.az.fx.ScreensController;
import heren.serv.az.utils.FXMLPATH;
import heren.serv.az.utils.FillListView;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Rashad Javadov
 */
public class OrderController extends MainStage implements Initializable, ControlledScreen {

    ScreensController myController;
    public int from, to;
    @FXML
    public static VBox vboxlist;

    @FXML
    FlowPane catPane, subCatPane;

    @FXML
    Label upper, down;

    public OrderController() {
        from = 0;
        to = 13;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        CScreenMenu cs = rs.getScreenMenu("2");
        System.out.println(" - - - - - " + cs.getCsmCategories().size());

        FillListView.fillList(from, to, vboxlist);
        //category button
        CategoryButton cbutton = new CategoryButton(subCatPane, catPane);
        catPane.getChildren().add(cbutton);
        for (CScreenMenuCategory c : cs.getCsmCategories()) {
            CategoryButton scbutton = new CategoryButton(subCatPane, catPane);
            scbutton.setCategory(c);
            scbutton.getBottomLbl().setText(c.getName());
            catPane.getChildren().add(scbutton);
        }
//scroll up and down 
        upper.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                to = to - 1;
                from = from + 1;
                System.out.println(to);
                FillListView.fillList(from, to, vboxlist);
            }
        });
        down.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                to = to - 1;
                from = from + 1;
                System.out.println(to);
                FillListView.fillList(from, to, vboxlist);
            }
        });
    }

    @FXML
    public void processClick(MouseEvent event) {
        LeftBtnPanel panel = new LeftBtnPanel();
        ((HBox) ((AnchorPane) event.getSource()).getParent().getParent().lookup("#leftBtnBox")).getChildren().clear();
        ((HBox) ((AnchorPane) event.getSource()).getParent().getParent().lookup("#leftBtnBox")).getChildren().add(panel);

        RightBtnPanel rightBtnPanel = new RightBtnPanel();
        ((HBox) ((AnchorPane) event.getSource()).getParent()).getChildren().clear();
//        ((HBox) ((AnchorPane) event.getSource()).getParent()).getChildren().add(rightBtnPanel);
    }

    @FXML
    public void goBack(MouseEvent event) {
        System.out.println("goBack");
        settingStage.configureStage(mainStage, FXMLPATH.departmentFXML);
    }

    @Override
    public void setScreenParent(ScreensController screenParent) {
        myController = screenParent;
    }

}
