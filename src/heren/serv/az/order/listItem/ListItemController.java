/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.order.listItem;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author rasha_000
 */
public class ListItemController extends AnchorPane {

    @FXML
    Label firstLab, secLab;

    @FXML
    public void okBtnHandle() {
    }

    public ListItemController() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("listItem.fxml"));
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void initialize(URL url, ResourceBundle rb) {

    }

    public Label getFirstLab() {
        return firstLab;
    }

    public void setFirstLab(Label firstLab) {
        this.firstLab = firstLab;
    }

    public Label getSecLab() {
        return secLab;
    }

    public void setSecLab(Label secLab) {
        this.secLab = secLab;
    }

}
