package heren.serv.az.fx;

import heren.serv.az.fx.ScreensController;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Angie
 */
public class ScreensFramework extends Application {

    public static String screen1ID = "LoginPane";
    public static String screen1File = "LoginPane.fxml";
    public static String screen2ID = "DepartmentPane";
    public static String screen2File = "DepartmentPane.fxml";
    public static String screen3ID = "OrderDetails";
    public static String screen3File = "OrderDetails.fxml";

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setFullScreen(true);

        ScreensController mainContainer = new ScreensController();
        mainContainer.loadScreen(ScreensFramework.screen1ID, ScreensFramework.screen1File);
        mainContainer.loadScreen(ScreensFramework.screen2ID, ScreensFramework.screen2File);
        mainContainer.loadScreen(ScreensFramework.screen3ID, ScreensFramework.screen3File);

        mainContainer.setScreen(ScreensFramework.screen1ID);

        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Scene scene = new Scene(root);
//        primaryStage.initStyle(StageStyle.TRANSPARENT);
//        primaryStage.initModality(Modality.NONE);
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
