/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.fx;

import heren.serv.az.utils.StageSetting;
import heren.serv.client.service.RestoranService;
import javafx.stage.Stage;

/**
 *
 * @author Mobby
 */
public class MainStage {

    public static RestoranService rs;
    public static Stage mainStage;
    public static StageSetting settingStage = new StageSetting();
    public static int addCategory = 0;

    public static Stage getMainStage() {
        return mainStage;
    }

    public static void setMainStage(Stage mainStage) {
        MainStage.mainStage = mainStage;
    }

    public StageSetting getSettingStage() {
        return settingStage;
    }

    public void setSettingStage(StageSetting settingStage) {
        this.settingStage = settingStage;
    }

}
