/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.category;

import heren.rest.common.client.CScreenMenuCategory;
import heren.rest.common.client.CScreenMenuItem;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;

/**
 * FXML Controller class
 *
 * @author Mobby
 */
public class CategoryButton extends AnchorPane {

    public CScreenMenuCategory category;
    public CScreenMenuItem menu = null;
    public FlowPane subCatPane, catPane;
    @FXML
    Label bottomLbl;

    public CategoryButton(FlowPane ssubCatPane, FlowPane ccatPane) {
        try {
            subCatPane = ssubCatPane;
            catPane = ccatPane;
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/heren/serv/az/category/category.fxml"));
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @FXML
    public void actionClick(MouseEvent event) {
//        System.out.println(((AnchorPane) event.getSource()).getId());
//        String lab = ((AnchorPane) event.getSource()).getId();
//        System.out.println(category.getName());
        subCatPane.getChildren().clear();
        if (category != null) {
            for (CScreenMenuItem menu : category.getcScreenMenuItems()) {
                SubCategoryButton scbutton = new SubCategoryButton(subCatPane, catPane);
                scbutton.setMenuItem(menu);
                scbutton.getBottomLbl().setText(menu.getName());
                subCatPane.getChildren().add(scbutton);
            }
        } else {
            if (menu != null) {
                for (CScreenMenuItem m : menu.getCsreenMenuItems()) {
                    SubCategoryButton scbutton = new SubCategoryButton(subCatPane, catPane);
                    scbutton.setMenuItem(m);
                    scbutton.getBottomLbl().setText(m.getName());
                    subCatPane.getChildren().add(scbutton);
                }
            }
        }
    }

    public Label getBottomLbl() {
        return bottomLbl;
    }

    public void setBottomLbl(Label bottomLbl) {
        this.bottomLbl = bottomLbl;
    }

    public CScreenMenuCategory getCategory() {
        return category;
    }

    public void setCategory(CScreenMenuCategory category) {
        this.category = category;
    }

    public CScreenMenuItem getMenu() {
        return menu;
    }

    public void setMenu(CScreenMenuItem menu) {
        this.menu = menu;
    }

}
