/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.category;

import heren.rest.common.client.CScreenMenuCategory;
import heren.rest.common.client.CScreenMenuItem;
import heren.serv.az.fx.MainStage;
import static heren.serv.az.fx.MainStage.addCategory;
import heren.serv.az.order.OrderController;
import heren.serv.az.order.listItem.ListItemController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;

/**
 * FXML Controller class
 *
 * @author Mobby
 */
public class SubCategoryButton extends AnchorPane {

    public CScreenMenuItem menuItem;
    public FlowPane subCatPane, catPane;
    @FXML
    Label bottomLbl;

    public SubCategoryButton(FlowPane ssubCatPane, FlowPane ccatPane) {
        try {
            subCatPane = ssubCatPane;
            catPane = ccatPane;
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/heren/serv/az/category/subCategory.fxml"));
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @FXML
    public void actionClick(MouseEvent event) {
        System.out.println("MouseEvent subMenu " + menuItem); 
            if (addCategory == 0) {
                catPane.getChildren().clear();
                CategoryButton cbutton = new CategoryButton(subCatPane, catPane);
                catPane.getChildren().add(cbutton);
            } 

        if (menuItem != null && menuItem.getCsreenMenuItems() != null && menuItem.getCsreenMenuItems().size() > 0) {
            addCategory++;
            System.out.println("xx xxx xx " + addCategory);
            CategoryButton cb = new CategoryButton(subCatPane, catPane);
            cb.getBottomLbl().setText(menuItem.getName());
            cb.setMenu(menuItem);
            subCatPane.getChildren().clear();
            catPane.getChildren().add(cb);
//catPane.
            System.out.println("here");
            for (CScreenMenuItem c : menuItem.getCsreenMenuItems()) {
                System.out.println("" + c.getName());
                SubCategoryButton scbutton = new SubCategoryButton(subCatPane, catPane);
                scbutton.getBottomLbl().setText(c.getName());
                scbutton.setMenuItem(c);
                subCatPane.getChildren().add(scbutton);
            }
        } else {
            addCategory = 0;
             final ListItemController lic = new ListItemController();
             lic.getFirstLab().setText(menuItem.getName());
             lic.setPrefWidth(OrderController.vboxlist.getPrefWidth());
             OrderController.vboxlist.getChildren().add(lic);
            System.out.println("gonna add to list");
        }
    }

    public Label getBottomLbl() {
        return bottomLbl;
    }

    public void setBottomLbl(Label bottomLbl) {
        this.bottomLbl = bottomLbl;
    }

    public CScreenMenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(CScreenMenuItem menuItem) {
        this.menuItem = menuItem;
    }

}
