/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.az.login;

import heren.serv.az.fx.ControlledScreen;
import heren.serv.az.fx.MainStage;
import heren.serv.az.fx.ScreensController;
import heren.serv.az.utils.FXMLPATH;
import heren.serv.client.src.ServiceClient;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Rashad Javadov
 */
public class LoginController extends MainStage implements Initializable, ControlledScreen {

    ScreensController myController;

    @FXML
    PasswordField passw;

    @FXML
    private void handleButtonAction() {
        settingStage.configureStage(mainStage, FXMLPATH.departmentFXML);
    }

    @FXML
    private void closeApp() {
        mainStage.close();
    }

    @FXML
    public void actionClick(MouseEvent event) {
        System.out.println(((Label) event.getSource()).getText());
        String lab = ((Label) event.getSource()).getText();
        if (!lab.equalsIgnoreCase("")) {
            passw.setText(passw.getText() + lab);
        } else {
            passw.setText("");
        }
    }

    @FXML
    private void getGetNumBtnText(ActionEvent event) {
//        Label lbl = (Label) event.getSource().
//        passw.setText(passw.getText() + numBtn.getText());
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        rs = new ServiceClient("85.132.24.74:57799");
        rs = new ServiceClient("12.0.3.205:8080");
        System.out.println(rs);
    }

    public void setScreenParent(ScreensController screenParent) {
        myController = screenParent;
    }

}
