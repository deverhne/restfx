/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.client.src;

import heren.rest.common.client.CDep;
import heren.rest.common.client.CDepartment;
import heren.rest.common.client.CScreenMenu ;
import heren.rest.common.client.CMyTable;
import heren.serv.client.service.RestoranService;
import java.util.List;
import java.util.logging.Logger;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
 
/**
 *
 * @author Rashad Javadov
 */
public class ServiceClient implements RestoranService {

    private static final Logger LOG = Logger.getLogger(ServiceClient.class.getName());

    private WebTarget webTarget;
    private Client client;
    private static String BASE_URI;

    public ServiceClient() {

    }

    public ServiceClient(String ip) {
        client = ClientBuilder.newClient();
        BASE_URI = "http://" + ip + "/rest-ws-app/rest/";
        LOG.info(client.getConfiguration().toString());
    }

    public List<CDepartment> getDepartments(String idDepartment) throws ClientErrorException {
        webTarget = client.target(BASE_URI).path("dict/department");
        String[] queryParamNames = new String[]{"idDepartment"};
        String[] queryParamValues = new String[]{idDepartment};
        setAdditionalParams(queryParamNames, queryParamValues);
        getQueryOrFormParams(queryParamNames, queryParamValues);
        GenericType<List<CDepartment>> gType = new GenericType<List<CDepartment>>() {
        };
        return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(gType);
    }

    public CScreenMenu  getScreenMenu(String idDepartment) throws ClientErrorException {
        webTarget = client.target(BASE_URI).path("dict/allMenu");
        String[] queryParamNames = new String[]{"idDepartment"};
        String[] queryParamValues = new String[]{idDepartment};
        setAdditionalParams(queryParamNames, queryParamValues);
        getQueryOrFormParams(queryParamNames, queryParamValues);
        return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(CScreenMenu .class);
    }

    public CDep getRestaurans() throws ClientErrorException {
        return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(CDep.class);
    }

    public List<CMyTable> getMyTablesInDepartment(String idDepartment) throws ClientErrorException {
        webTarget = client.target(BASE_URI).path("dict/tables");
        String[] queryParamNames = new String[]{"idDepartment"};
        String[] queryParamValues = new String[]{idDepartment};
        setAdditionalParams(queryParamNames, queryParamValues);
        getQueryOrFormParams(queryParamNames, queryParamValues);
        return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(new GenericType<List<CMyTable>>() {
        });
    }

    public String getProductDiscount(String idProduct) throws ClientErrorException {
        webTarget = client.target(BASE_URI).path("order/productDiscount");
        String[] queryParamNames = new String[]{"idProduct"};
        String[] queryParamValues = new String[]{idProduct};
        setAdditionalParams(queryParamNames, queryParamValues);
        return webTarget.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
    }

    public String getProductServiceOrdering(String idProduct) throws ClientErrorException {
        webTarget = client.target(BASE_URI).path("order/productServiceOrdering");
        String[] queryParamNames = new String[]{"idProduct"};
        String[] queryParamValues = new String[]{idProduct};
        return webTarget.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
    }

    public String getTaxTemplateRoundValue(String idDepartment) throws ClientErrorException {
        webTarget = client.target(BASE_URI).path("order/taxTemplate");
        String[] queryParamNames = new String[]{"idDepartment"};
        String[] queryParamValues = new String[]{idDepartment};
        ;
        setAdditionalParams(queryParamNames, queryParamValues);
        return webTarget.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
    }

    public String getProductPriceMapResult(String idDepartment, String idSMItem, String idSMCategory) throws ClientErrorException {
        webTarget = client.target(BASE_URI).path("order/ProductPriceMapResult");
        String[] queryParamNames = new String[]{"idDepartment", "idSMItem", "idSMCategory"};
        String[] queryParamValues = new String[]{idDepartment, idSMItem, idSMCategory};
        ;
        setAdditionalParams(queryParamNames, queryParamValues);
        return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(String.class);
    }

    public String getProductPrice(String idMenuItem, String idPPDefinetion) throws ClientErrorException {
        webTarget = client.target(BASE_URI).path("order/productPrice");
        String[] queryParamNames = new String[]{"idMenuItem", "idPPDefinetion"};
        String[] queryParamValues = new String[]{idMenuItem, idPPDefinetion};
        ;
        setAdditionalParams(queryParamNames, queryParamValues);
        return webTarget.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
    }

    private void setAdditionalParams(String[] queryParamNames, String[] queryParamValues) {
        javax.ws.rs.core.Form form = getQueryOrFormParams(queryParamNames, queryParamValues);
        javax.ws.rs.core.MultivaluedMap<String, String> map = form.asMap();
        for (java.util.Map.Entry<String, java.util.List<String>> entry : map.entrySet()) {
            java.util.List<String> list = entry.getValue();
            String[] values = list.toArray(new String[list.size()]);
            webTarget = webTarget.queryParam(entry.getKey(), (Object[]) values);
        }
    }

    private Form getQueryOrFormParams(String[] paramNames, String[] paramValues) {
        Form form = new javax.ws.rs.core.Form();
        for (int i = 0; i < paramNames.length; i++) {
            if (paramValues[i] != null) {
                form = form.param(paramNames[i], paramValues[i]);
            }
        }
        return form;
    }

    public void close() {
        client.close();
    }

}
