/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package heren.serv.client.service;

import heren.rest.common.client.CDep;
import heren.rest.common.client.CDepartment;
import heren.rest.common.client.CScreenMenu ;
import heren.rest.common.client.CMyTable;
import java.util.List;

/**
 *
 * @author Rashad Javadov
 */
public interface RestoranService {

    public List<CDepartment> getDepartments(String idDepartment);

    public CScreenMenu  getScreenMenu(String idDepartment);

    public CDep getRestaurans();

    public List<CMyTable> getMyTablesInDepartment(String idDepartment);

    public String getProductDiscount(String idProduct);

    public String getProductServiceOrdering(String idProduct);

    public String getTaxTemplateRoundValue(String idDepartment);

    public String getProductPriceMapResult(String idDepartment, String idSMItem, String idSMCategory);

    public String getProductPrice(String idMenuItem, String idPPDefinetion);
}
